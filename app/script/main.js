'use strict';

$(document).ready(function () {


    //Плавный скролл до элемента

    $('.scroll a[href^="#"]').click(function () {
        var elementClick = $(this).attr("href");
        //console.log(destination)
        var destination = $(elementClick).offset().top;
        console.log(destination);
        jQuery("html:not(:animated),body:not(:animated)").animate({scrollTop: destination}, 1100);
        return false;
    });





    //Слайдер с вопросами
    $('.b-sc-6-slider').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1
    });

    //Слайдер с парнерами
    $('.b-sc7-slider').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 6,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1100,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
            {
                breakpoint: 850,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
            {
                breakpoint: 550,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true
                }
            }
        ]
    });

    //Слайдер с грамотами
    $('.b-sc9-slider').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1050,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
            {
                breakpoint: 850,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
            {
                breakpoint: 550,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true
                }
            }
        ]
    });


    // Фотогаллеря в первом слайдере

    $('.b-sc1-sl-i-photo').each(function () {

        var photoDiv = $(this);

        var photoClass = new Photo();

        photoClass.getAllSlide(photoDiv);

        photoClass.showFistSlide(photoDiv);

        photoClass.addEvent(photoDiv);


    });



    //Первый слайдер

    var fistSlider = new FistSlider();

    fistSlider.sliderInit();

    fistSlider.bildSliderMenu();

    fistSlider.getActiveSlide();

    fistSlider.subscribeOnEvents();

    fistSlider.subscribeOnClickLabel();

    fistSlider.subscribeOnSwipe();


    //Модальные окна

    $('#test-modal').on('shown.bs.modal', function () {
        $('#myInput').focus()
    })
});


//
//Класс фотогаллереи на первом слайде
//

function Photo() {

    var _this = this;

    _this.getAllSlide = function (photoDiv) {

        var data = photoDiv.find('.b-sc1-sl-img-slide');

        data.each(function () {

            var itemPrev = '<img class="b-sc1-sl-i-photo-sm-item"' +
                ' data-big="' + $(this).attr('data-big') + '" ' +
                'src="' + $(this).attr('data-prev') + '" alt=""/>';

            photoDiv.find('.b-sc1-sl-i-photo__small').append(itemPrev)
        });
    };

    _this.showFistSlide = function (photoDiv) {
        var fist = photoDiv.find('.b-sc1-sl-i-photo-sm-item').eq(0);
        $('.b-sc1-sl-i-photo__big').css('background-image', 'url('+fist.attr('data-big')+')')
    };

    _this.addEvent = function(photoDiv){
        photoDiv.find('.b-sc1-sl-i-photo-sm-item').each(function(){
           $(this).on('click', function(){
               photoDiv.find('.b-sc1-sl-i-photo__big').css('background-image', 'url('+$(this).attr('data-big')+')')
           });
        });
    };
}


//
//Класс первого слайдера
//

function FistSlider(){
    var _this = this;

    var sliderDiv = $('.b-sc1-slider-body');

    _this.sliderInit = function(){
        sliderDiv.slick({
            dots: false,
            infinite: false,
            arrows: false,
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1
        });
    };

    _this.bildSliderMenu = function(){

        $('.b-sc1-slider-body__item').each(function(){
            var parentDiv = $(".b-sc1-sl-h-wrap");

            var itemMenu = '<div data-id="'+$(this).attr('data-id')+'" class="b-sc1-sl-h-item">' +
                '<p class="b-sc1-sl-h-item__p">'+$(this).attr('data-head-content')+'</p>' +
                ' </div>';
            parentDiv.append(itemMenu)
        })
    };

    _this.getActiveSlide = function(){
        var numberActiveSlide = sliderDiv.slick('slickCurrentSlide');
        var labels = $('.b-sc1-sl-h-item');
        labels.removeClass('b-sc1-sl-h-item--active');
        labels.eq(numberActiveSlide).addClass('b-sc1-sl-h-item--active');

        _this.calculateOffsetWrapper();
    };

    _this.subscribeOnEvents = function(){
        _this.subscribeOnClickLeftArr();
        _this.subscribeOnClickRightArr();
    };

    _this.subscribeOnClickLeftArr = function(){
        $('.b-sc1-slider-header__left').on('click', function(){
            _this.slickPrev();
        })
    };

    _this.subscribeOnClickRightArr = function(){
        $('.b-sc1-slider-header__right').on('click', function(){
            _this.slickNext();
        })
    };

    _this.slickNext = function(){
        sliderDiv.slick('slickNext');
        _this.getActiveSlide();
    };

    _this.slickPrev = function(){
        sliderDiv.slick('slickPrev');
        _this.getActiveSlide();
    };

    _this.subscribeOnClickLabel = function(){
        $('.b-sc1-sl-h-item').on('click', function(){
            var data = $(this).attr('data-id');

            sliderDiv.slick('slickGoTo', data);

            _this.getActiveSlide();
        })
    };

    _this.subscribeOnSwipe = function(){
        sliderDiv.on('swipe', function(){
            _this.getActiveSlide();
        })
    };

    _this.calculateOffsetWrapper = function(){
        var activeLabelId = $('.b-sc1-sl-h-item--active').attr('data-id');

        var allLabel = $('.b-sc1-sl-h-item');

        var offset = 0;

        for(var i = 0; i < activeLabelId; i++){
            offset += allLabel.eq(i).outerWidth(true);
        };

        $('.b-sc1-sl-h-wrap').css('left', '-'+offset+'px')

    }
}